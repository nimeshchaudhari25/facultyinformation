<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

  public function __construct() {

    parent::__construct();

    // load base_url
    $this->load->helper('url');
	
	/*load database libray manually*/
	$this->load->database();
	
	/*load Model*/
	$this->load->model('Registration_model');
	
	/*load Session*/
	$this->load->library('session');
	
  }

  public function index(){

	/*load registration view form*/
    $this->load->view('v_registration'); 
	
	/*Check submit button */
		if($this->input->post('signup'))
		{
		    $data['user_name']=$this->input->post('name');
			$data['user_phoneno']=$this->input->post('phone');
			$data['user_email_id']=$this->input->post('email');		
			$data['user_password']=$this->input->post('pass');	
			$data['user_status']='1';	
			
			$response=$this->Registration_model->signup($data);
			if($response==true){
				echo "Your registration has been successfully completed.";
			       //$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your registration has been successfully completed.</div>');				
				   $this->load->view('v_registration','refresh');
			}
			else{
					//$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">There is error in registration! Please try again later</div>');
					echo "There is error in registration! Please try again later";
					 $this->load->view('v_registration','refresh');
				
			}
		}
  }
 
}

