<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
	<?php include_once("header_includes.php"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">    
     <?php include_once('admin_header.php'); ?>    
	 <?php include_once('side_menubar.php'); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="padding-left: 40px;">
          <h1>
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content" style="padding-left: 40px;">
          <!-- Small boxes (Stat box) -->
  <div class="">								
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Edit Profile </h3>
                </div><!-- /.box-header
                <div class="box-body"> -->
							<div class="">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
								<h4></h4>
                  <form role="form" action="template_insert.php" method="post" enctype="multipart/form-data" >
                    <!-- text input -->
					<div class="row">
						<div class="col-sm-1">
							<div class="form-group">
							  <label>Title<span style="color:#FF0000">*</span></label>
							   <select name="sal" style="max-width:90%";>
									<option selected="selected" value="Mr">Mr.</option>
									<option value="Mrs">Mrs.</option>
									<option value="Miss">Miss</option>
							   </select>
							</div>
							
						</div> 
						<div class="col-sm-3">
							<div class="form-group">
							  <label>First Name<span style="color:#FF0000">*</span></label>
							  <input type="text" name="fname" id="fname"  class="form-control" placeholder="Enter First Name" required />
							</div>
						</div> 
						<div class="col-sm-3">
							<div class="form-group">
							  <label>Middle Name<span style="color:#FF0000">*</span></label>
							  <input type="text" name="mname" id="mname"  class="form-control" placeholder="Enter Middle Name" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Last Name<span style="color:#FF0000">*</span></label>
							  <input type="text" name="lname" id="lname"  class="form-control" placeholder="Enter Last Name" required />
							</div>
						</div> 
					 </div><!-- /.box-body -->
					 <!-- text input -->
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Date of Birth<span style="color:#FF0000">*</span></label>
							  <input type="date" name="fname" id="fname"  class="form-control"  placeholder="Enter First Name" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Email ID<span style="color:#FF0000">*</span></label>
							  <input type="text" name="emailid" id="emailid"  class="form-control" placeholder="Enter Email ID" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Mobile No<span style="color:#FF0000">*</span></label>
							  <input type="text" name="lname" id="lname"  class="form-control" placeholder="Enter Mobile No" required />
							</div>
						</div> 
					 </div><!-- /.box-body -->
					  <!-- text input -->
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Father Name<span style="color:#FF0000">*</span></label>
							  <input type="text" name="fname" id="fname"  class="form-control" placeholder="Enter Father Name" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Mother Name<span style="color:#FF0000">*</span></label>
							  <input type="text" name="mname" id="mname"  class="form-control" placeholder="Enter Mother Name" required />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Gender<span style="color:#FF0000">*</span></label>
							  <div class="row">
								<div class="col-sm-6">
								<input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Male
								</label>
								</div>
								<div class="col-sm-6">
								<input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Female
							    </label>
								</div>
							  </div>
							</div>
						</div> 
						
					 </div><!-- /.box-body -->
					 <div class="row">
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Place Of Birth : <span style="color:#FF0000">*</span></label>
							  <input type="date" name="pbirth" id="pbirth"  class="form-control"  required />
							 
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Image: <span style="color:#FF0000">*</span></label>
							  <input type="File" name="Degree" id="Degree"  class="form-control" placeholder="Enter Degree" required />
							 
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label  style="height: 60px; width: 30%;
									  border: 1px solid #4CAF50;" >
							</label>
							 
							 
							</div>
						</div> 
					 </div>
					 <div class="row">
						<div class="col-sm-12">
							<div class="form-group">
							  <label>Marital Status : <span style="color:#FF0000">*</span></label>
							   <div class="row">
								<div class="col-sm-6">
								<input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Single 
								</label>
								<input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Married
								</label>
								<input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Divorced
								</label>
							</div>
							  </div>
							 
							</div>
						</div> 
					</div> 
					  <div class="row">
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Nationality : <span style="color:#FF0000">*</span></label>
							  <input type="text" name="Degree" id="Degree"  class="form-control" placeholder="Enter Nationality" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Category : <span style="color:#FF0000">*</span></label>
							  <input type="text" name="Degree" id="Degree"  class="form-control" placeholder="Enter Category" required />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Current Designation :</label>
							   <input type="text" name="Degree" id="Degree"  class="form-control" placeholder="Enter Current Designation" required />
							  
							</div>
						</div>							
						<div class="col-sm-12">
							<div class="box-footer">
								<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
								<button type="button" name="cancel" id="cancel" class="btn btn-danger">Cancel</button>
							</div>
						</div>						
					 </div><!-- /.box-body --<!-- /.box-body -->
					
                  </form>
               
              </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">    
      </footer>  
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

	<?php include_once("footer_includes.php"); ?>
    
  </body>
</html>