<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
	<?php include_once("header_includes.php"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">    
     <?php include_once('admin_header.php'); ?>    
	 <?php include_once('side_menubar.php'); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="padding-left: 40px;">
          <h1>
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Academic</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content" style="padding-left: 40px;">
          <!-- Small boxes (Stat box) -->
  <div class="">								
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Add Subject Books Published By Other Local Publisher/National Level Chapters in Books Published .</h3>
                </div><!-- /.box-header
                <div class="box-body"> -->
							<div class="">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
								<h4></h4>
                  <form role="form" action="template_insert.php" method="post" enctype="multipart/form-data" >
                    <!-- text input -->
					
					 <!-- text input -->
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Title with page no<span style="color:#FF0000">*</span></label>
							 <input type="text" name="nameofcourse" id="nameofcourse"  class="form-control" placeholder="Enter Name Of Course" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Book Title Editor & Publisher<span style="color:#FF0000">*</span></label>
							  <input type="text" name="Stream" id="Stream"  class="form-control" placeholder="Enter Stream" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>ISSN No<span style="color:#FF0000">*</span></label>
							  <input type="text" name="NoofStudents" id="NoofStudents"  class="form-control" placeholder="Enter No of Students" required />
							</div>
						</div>
						
					 </div><!-- /.box-body -->
					 <!-- text input -->
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Whether peer reviewed.imapact,if any<span style="color:#FF0000">*</span></label>
							  <input type="text" name="nooflectures" id="nooflectures"  class="form-control" placeholder="Enter No Of Lectures" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>No of Coauthors<span style="color:#FF0000">*</span></label>
							  <input type="text" name="PeriodsMissed" id="Periods Missed"  class="form-control" placeholder="Enter Periods Missed" required />
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="form-group">
							  <label>Whether you are the main author(Yes/NO)<span style="color:#FF0000">*</span></label>
							  <input type="text" name="year" id="year"  class="form-control" placeholder="Enter Year" required />
							</div>
						</div>
						
					 </div><!-- /.box-body -->
					 
					
					
					  <div class="row">						
						<div class="text-right col-sm-12" >
							<div class="form-group" style="padding-right: 10px;">					
							  <label><a href="#">Add More</a></label>						
							</div>
						</div>
						<div class="col-sm-12">
							<div class="box-footer">
								<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
								<button type="button" name="cancel" id="cancel" class="btn btn-danger">Cancel</button>
							</div>
						</div>						
					 </div><!-- /.box-body --<!-- /.box-body -->
					
                  </form>
               
              </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">    
      </footer>  
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

	<?php include_once("footer_includes.php"); ?>
    
  </body>
</html>