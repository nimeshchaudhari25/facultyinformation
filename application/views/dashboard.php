<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>    
	<?php include_once("header_includes.php"); ?>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">      
      <?php include_once('admin_header.php'); ?>  
	 <?php include_once('side_menubar.php'); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="padding-left: 40px;">
          <h1> <small>Control panel</small>    </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content" style="padding-left: 40px;">
          <!-- Small boxes (Stat box) -->		  
					  <!-- text input -->
                 <form role="form" action="user_update.php" method="post" enctype="multipart/form-data" >
              
		<div class="row">
             <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<!-- <div class="form-group" style="text-align:right;">
					<a  style="background:#006400;text-align:right;" href="<?php //echo base_url();?>editprofile_add" class="btn btn-primary"> <i class="fa fa-plus" aria-hidden="true"></i> Add</a>
				     </div> -->
                  <table id="example2" class="table table-bordered table-hover">
                    <thead class="align-center">					
                      <tr >
						<th width="5%">Sr No</th>
						<th width="5%">First Name</th>
                        <th width="10%">Last Name</th>
						<th width="10%">Sure Name</th>
                        <th width="10%">Active Status</th>	
                        <th width="1%">Edit</th>
                        <th width="1%">Delete</th>
						<th width="1%">View</th>
						<th width="1%">PDF</th>
						 
                      </tr>
                    </thead>
                    <tbody>
						<tr style="">
						<td  contenteditable="true" id="">1</td>
						<td  contenteditable="true" id="">Nimeshkumar</td>
						<td  contenteditable="true" id="">Nandraybhai</td>
						<td  contenteditable="true" id="">Chaudhari</td>
						<td>Active</td>
						<td width="2%">
							<a href="" title="Edit" class="btn btn-warning">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a onclick="" class="btn btn-danger" href="" title="Delete">
							<i class="fa fa-times" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-success">
							<i class="fa fa-eye" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-danger">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
							</a>
						</td>
						</tr>
	 <!-- End 1 --> 
     <!-- Start 2 -->
						<tr style="">
						<td  contenteditable="true" id="">2</td>
						<td  contenteditable="true" id="">Nimeshkumar</td>
						<td  contenteditable="true" id="">Nandraybhai</td>
						<td  contenteditable="true" id="">Chaudhari</td>
						<td>Active</td>
						<td>
							<a href="" title="Edit" class="btn btn-warning">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a onclick="" class="btn btn-danger" href="" title="Delete">
							<i class="fa fa-times" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-success">
							<i class="fa fa-eye" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-danger">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
							</a>
						</td>
						</tr>	
		 <!-- End 2 --> 
     <!-- Start 3 -->
						<tr style="">
						<td  contenteditable="true" id="">3</td>
						<td  contenteditable="true" id="">Nimeshkumar</td>
						<td  contenteditable="true" id="">Nandraybhai</td>
						<td  contenteditable="true" id="">Chaudhari</td>
						<td>Active</td>
						<td>
							<a href="" title="Edit" class="btn btn-warning">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a onclick="" class="btn btn-danger" href="" title="Delete">
							<i class="fa fa-times" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="" class="btn btn-success">
							<i class="fa fa-eye" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="" class="btn btn-danger">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
							</a>
						</td>
						</tr>
 <!-- End 3 --> 
     <!-- Start 4 -->
						<tr style="">
						<td  contenteditable="true" id="">4</td>
						<td  contenteditable="true" id="">Nimeshkumar</td>
						<td  contenteditable="true" id="">Nandraybhai</td>
						<td  contenteditable="true" id="">Chaudhari</td>
						<td>Active</td>
						<td>
							<a href="" title="Edit" class="btn btn-warning">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a onclick="" class="btn btn-danger" href="" title="Delete">
							<i class="fa fa-times" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-success">
							<i class="fa fa-eye" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-danger">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
							</a>
						</td>
						</tr>	
 <!-- End 4 --> 
     <!-- Start 5 -->
						<tr style="">
						<td  contenteditable="true" id="">5</td>
						<td  contenteditable="true" id="">Nimeshkumar</td>
						<td  contenteditable="true" id="">Nandraybhai</td>
						<td  contenteditable="true" id="">Chaudhari</td>
						<td>Active</td>
						<td>
							<a href="" title="Edit" class="btn btn-warning">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a onclick="" class="btn btn-danger" href="" title="Delete">
							<i class="fa fa-times" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-success">
							<i class="fa fa-eye" aria-hidden="true"></i> 
							</a>
						</td>
						<td>
							<a href="" title="View Template" class="btn btn-danger">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
							</a>
						</td>
						</tr>							
                    </tbody>
                    <tfoot>
                       <tr>
                        <th>Sr No</th>
                        <th>First Name</th>
						<th>Last Name</th> 
					    <th>Sure Name</th>
                        <th>Active Status</th>
                        <th>Edit</th>
                        <th>Delete</th>
						<th>View</th> 
						<th>PDF</th>
                      </tr>
                    </tfoot>
                  </table>
				  
				  <!-- pagination starts -->
				  
				  	
				 <!-- pagination ends -->
				  </form>
                </div><!-- /.box-body -->   
              </div><!-- /.box --> 
           </div><!-- /.col --> 
          </div><!-- /.row -->        
	  
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">    
      </footer>  
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->
<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        Accordion Item #1
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Accordion Item #2
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        Accordion Item #3
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>
</div>
	<?php include_once("footer_includes.php"); ?>
  
  </body>
</html>