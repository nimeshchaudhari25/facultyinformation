 <!-- Left side column. contains the logo and sidebar 
      <aside class="main-sidebar" style="width:130px;">
        <!-- sidebar: style can be found in sidebar.less -->
		<?php include_once("header_includes.php"); ?>
		<aside class="main-sidebar" >
        <section class="sidebar">          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" id="myDIV" >
            <li class="header" style="color:#fff" >MAIN NAVIGATION</li>
            <li class="nav-item ">
				<a href="<?php echo base_url();?>Dashboard">
					<i class="fa fa-user" aria-hidden="true"></i> 
					<span>Dashboard</span>
				</a>
			</li>
			<li class="nav-item active">
				<a href="<?php echo base_url();?>Profile" class="nav-link">
					<i class="fa fa-user" aria-hidden="true"></i>
					<span>Edit Profile</span>
                </a>
			</li>
			<li class="nav-item">
			  <a href="<?php echo base_url();?>Academiccredentials">
                <i class="fa fa-user" aria-hidden="true"></i>
				<span>Acadamic Credentials</span>
              </a>
		   </li>
		  <!-- <li>
			  <a href="<?php echo base_url();?>">
                <i class="fa fa-user" aria-hidden="true"></i>
				<span>Whether Acquired any degree </br>&nbsp &nbsp &nbsp &nbsp or fresh academic qualifications</br>&nbsp &nbsp &nbsp &nbsp during the year</span>
              </a>
			</li> -->
			<li>
			  <a href="<?php echo base_url();?>Appointments">
                <i class="fa fa-user" aria-hidden="true"></i> 
				<span>Appointments held prior </br> &nbsp &nbsp &nbsp &nbsp  to joining this institute</span>
              </a>
			</li>
			<li>
			  <a href="<?php echo base_url();?>Orientation">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Academic Staff College </br>&nbsp &nbsp &nbsp &nbsp  Orientation/Refresher </br>&nbsp &nbsp &nbsp &nbsp  Course attended During the Year</span>
              </a>
			   </li><li>
			  <a href="<?php echo base_url();?>TeachingEngagement">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Teaching Engagement</span>
              </a>
			   </li><li>
			  <a href="<?php echo base_url();?>ResearchPaper">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Research published papers</span>
              </a>
			   </li>
			   <li>
			  <a href="<?php echo base_url();?>ResearchPaperOtherPublications">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Publications other than </br>&nbsp &nbsp &nbsp &nbsp Journal</span>
              </a>
			   </li> 
			   <li>
			  <a href="<?php echo base_url();?>BookPublication">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Subject Books Published </br>&nbsp &nbsp &nbsp &nbsp  By Other Local Publisher/National</br>&nbsp &nbsp &nbsp &nbsp  Level Chapters in Books Published</span>
              </a>
			   </li>
			   <li>
			  <a href="<?php echo base_url();?>MajorProjects">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Research Projects(a) Major Project</span>
              </a>
			   </li><li>
			  <a href="<?php echo base_url();?>ConsultancyProjects">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Research Projects(a) Consultancy</span>
              </a>
			   </li><li>
			  <a href="<?php echo base_url();?>Award">
                <i class="fa fa-user" aria-hidden="true"></i> <span>National/State/University </br>&nbsp &nbsp &nbsp &nbsp  Level Award from Academic </br>&nbsp &nbsp &nbsp &nbsp  Bodies/Associations</span>
              </a>
			   </li><li>
			  <a href="<?php echo base_url();?>InvitedLectures">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Invited Lectures/Papers</span>
              </a>
			   </li>
			   <li>
			  <a href="<?php echo base_url();?>Fdp">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Seminars,Conferences,Symposia,</br>&nbsp &nbsp &nbsp &nbsp  Workshops,FDPs etc Attended</span>
              </a>			  
            </li>
			</li>
			   <li>
			  <a href="<?php echo base_url();?>researchdegree">
                <i class="fa fa-user" aria-hidden="true"></i> <span>Research Degree(s)</span>
              </a>
			  
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
	<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("myDIV");
var btns = header.getElementsByClassName("nav-item");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}
</script>