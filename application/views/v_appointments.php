<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
	<?php include_once("header_includes.php"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">    
     <?php include_once('admin_header.php'); ?>    
	 <?php include_once('side_menubar.php'); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="padding-left: 40px;">
          <h1>
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Academic</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content" style="padding-left: 40px;">
          <!-- Small boxes (Stat box) -->
  <div class="">								
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Add Appointments held prior to joining this institution</h3>
                </div><!-- /.box-header
                <div class="box-body"> -->
							<div class="">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
								<h4></h4>
                  <form role="form" action="template_insert.php" method="post" enctype="multipart/form-data" >
                    <!-- text input -->
					
					 <!-- text input -->
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							  <label>Designation<span style="color:#FF0000">*</span></label>
							 <input type="text" name="designation" id="designation"  class="form-control" placeholder="Enter Designation" required />
							</div>
						</div> 
						<div class="col-sm-6">
							<div class="form-group">
							  <label>Name of Employer<span style="color:#FF0000">*</span></label>
							  <input type="text" name="nameofemployer" id="nameofemployer"  class="form-control" placeholder="Enter Name of Employer" required />
							</div>
						</div> 
						
					 </div><!-- /.box-body -->
					 <div class="row">
						<div class="col-sm-3">
							<div class="form-group">
							  <label>Date of Joining<span style="color:#FF0000">*</span></label>
							  <input type="date" name="dateofjoining" id="dateofjoining"  class="form-control" placeholder="Enter Date of Joining" required />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label>Date of Leaving<span style="color:#FF0000">*</span></label>
							  <input type="date" name="dateofleaving" id="dateofleaving"  class="form-control" placeholder="Enter Date of Leaving" required />
							</div>
						</div> 
						<div class="col-sm-3">
							<div class="form-group">
							  <label>Salary with Grade<span style="color:#FF0000">*</span></label>
							  <input type="text" name="salarywithgrade" id="salarywithgrade"  class="form-control" placeholder="Enter Salary with Grade" required />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label>Reason of Leaving<span style="color:#FF0000">*</span></label>
							  <input type="text" name="sponsoring" id="sponsoring"  class="form-control" placeholder="Enter Reason of Leaving" required />
							</div>
						</div> 
					 </div>
					
					
					  <div class="row">						
						<div class="text-right col-sm-12" >
							<div class="form-group" style="padding-right: 10px;">					
							  <label><a href="#">Add More</a></label>						
							</div>
						</div>
						<div class="col-sm-12">
							<div class="box-footer">
								<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
								<button type="button" name="cancel" id="cancel" class="btn btn-danger">Cancel</button>
							</div>
						</div>						
					 </div><!-- /.box-body --<!-- /.box-body -->
					
                  </form>
               
              </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">    
      </footer>  
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

	<?php include_once("footer_includes.php"); ?>
    
  </body>
</html>