$(document).ready(function(){
	var template_id =  $('#template_id').val();
	
	$('#table-header-color').css("top",$('#table-head').height()-30);
	
	$("#myTable").dragtable({dragHandle: ".dragHandle", persistState: function(table) { 
		table.el.find('th').each(function(i) { 
				if(this.id != '') {
					table.sortOrder[this.id]=i;
					$(this).attr('data-col-position',""+(i+1));
					
					var data_col = $(this).attr('data-col');
					var data_field = $(this).attr('data-field');
					var data_table = $(this).attr('data-table');
					var data_position = $(this).attr('data-col-position');
					var data_row = "0";
					var data_visibility = $(this).attr('data-visibility');
					var data_value = "0";
						
					updatedatachild(data_field,data_table,template_id,data_value,data_col,data_position,data_row,data_visibility);
				} 
		}); 
		//console.log( JSON.stringify(table.sortOrder) ); 
		}
	});	
	
	/* $( ".draggable-div" ).sortable({
        revert: true
    });
	
	$( ".draggable-div" ).draggable({
      connectToSortable: ".draggable-div",
      helper: "clone",
      revert: "invalid"
    }); */
	
	/*$(".draggable-div").sortable({
		connectWith: ".draggable-div",
		stop: function(event,ui){
			$('.draggable-div').removeClass('hover');
		},
		over: function(event,ui){
			//will add class .hover to list
			$('.ui-sortable-placeholder').parents('.draggable-div').addClass('hover');
		},
		out: function(event,ui){
			$('.ui-sortable-placeholder').parents('.draggable-div').removeClass('hover');
		},
		change: function(event, ui) {
			//will style placeholder
			$('.ui-sortable-placeholder').css({
				visibility: 'visible',
				background: '#EEE'
			});
		}*/
		/* ,
		update: function (event, ui) {
			var data = $(this).sortable('serialize');
			alert(data);
			POST to server using $.post or $.ajax
			 $.ajax({
				data: data,
				type: 'POST',
				url: '/your/url/here'
			}); 
		} */
	//});
	
	// containt-editable 	
	$(".content-editable").on("click", function(e){
		 e.preventDefault();
		 var el = $(this).parent().prev();
		 if(el.attr('contenteditable'))
		 {	
			var field= $(this).attr('data-field');
			var table= $(this).attr('data-table');
			var data_type= $(this).attr('data-type');
			var serialized= $(this).attr('data-serialized');
			var value= el.html();
			//alert(value);
			updatedata(field, table, template_id, value, serialized, data_type); 
		 }
		 (el.attr('contenteditable') ?
			el.removeAttr('contenteditable') :
			el.attr('contenteditable', true));	
	});
					
	
	$(".content-bold").on("click", function(e){
		 e.preventDefault();
		 var el = $(this).parent().prev();
		 el.hasClass('bold-text') ? el.removeClass('bold-text') : el.addClass('bold-text');
		var field= $(this).attr('data-field');
		var table= $(this).attr('data-table');
		var data_type= $(this).attr('data-type');
		var serialized= $(this).attr('data-serialized');
		var value= el.hasClass('bold-text') ? 1 : 0;
		updatedata(field, table, template_id, value, serialized, data_type); 
				
	});
	
	$(".content-bold-th").on("click", function(e){
		 e.preventDefault();
		 var el = $(this).parent().prev();
		 el.hasClass('bold-text') ? el.removeClass('bold-text') : el.addClass('bold-text');
		var data_field= $(this).attr('data-field');
		var data_col= $(this).attr('data-col');
		var data_position= $(this).attr('data-col-position');
		var data_table= $(this).attr('data-table');
		var data_visibility= $(this).attr('data-visible');
		var data_row= $(this).attr('data-type');
		var serialized= $(this).attr('data-serialized');
		var data_value= el.hasClass('bold-text') ? 1 : 0;
		//var data_row= '0';
		//updatedata(field, table, template_id, value, serialized, data_type); 
		//console.log(field+","+table+","+template_id+","+value+","+serialized+","+data_type);	
		updatedatachild(data_field,data_table,template_id,data_value,data_col,data_position,data_row,data_visibility);
	});
	
	// Font Color Change
	 $(".content-color-change").on("change",function(e){ 
          e.preventDefault();
		  var el = $(this).parent().parent().prev();								  
            el.css('color',$(this).val());
			var field= $(this).attr('data-field');
			var table= $(this).attr('data-table');
			var data_type= $(this).attr('data-type');
			var serialized= $(this).attr('data-serialized');
			var value= $(this).val();
			updatedata(field, table, template_id, value, serialized, data_type);
      });
	  
	  $(".content-color-change-th").on("change",function(e){ 
          e.preventDefault();
		  var el = $(this).parent().parent().prev();								  
            el.css('color',$(this).val());
			
			var data_field= $(this).attr('data-field');
			var data_col= $(this).attr('data-col');
			var data_position= $(this).attr('data-col-position');
			var data_table= $(this).attr('data-table');
			var data_visibility= $(this).attr('data-visible');
			var data_row= $(this).attr('data-type');
			var serialized= $(this).attr('data-serialized');
			var data_value= $(this).val();
			
			updatedatachild(data_field,data_table,template_id,data_value,data_col,data_position,data_row,data_visibility);
      });
	  
	  /* // Change background Color
	  $(".changebackground").on("change",function(e){ 
          e.preventDefault();
		   var el = $(this).parent().prev();		
           el.css('background',$(this).val());
      }); */
	  
	    // Eye Symbole Event
	       $(".content-graycolor").on("click",function(e){ 
            e.preventDefault();		  
			var el = $(this).parent().next();
			var data_header= parseInt($(this).attr('data-header'));
			if(data_header == 1)
			{
				el.next().hasClass("intro") ? el.next().removeClass("intro") : el.next().addClass("intro");
			}
			el.hasClass("intro") ? el.removeClass("intro") : el.addClass("intro");
			if($(this).hasClass("fa-eye"))
			{
				$(this).removeClass("fa-eye");
				$(this).addClass("fa-eye-slash");				
			}
			else if($(this).hasClass("fa-eye-slash"))
			{
				$(this).removeClass("fa-eye-slash") ; $(this).addClass("fa-eye");
			}
				
			var field= $(this).attr('data-field');
			var table= $(this).attr('data-table');
			var data_type= $(this).attr('data-type');
			var serialized= $(this).attr('data-serialized');
			var value= el.hasClass("intro") ? 0 : 1;
			//alert("field:"+field+"table:"+table+"data_type:"+data_type+"serialized:"+serialized+"value:"+value);
			updatedata(field, table, template_id, value, serialized, data_type);
      });
	  
	      // Table Sortable Function
	
        $('input[type="checkbox"]').click(function () {
			
			var id = $(this).attr('id'); 
		
            if ($(this).is(":checked")) {
				//$("#myTable").find("[data-td-id='" + id + "']").show();	
				//$("#myTable").find("[data-th-id='" + id + "']").show();
				//$("#myTable #column_"+id).attr('data-visibility','1');
				  $("#myTable [data-th-id='" + id + "']").attr('data-visibility','1');
				  $("#myTable [data-td-id='" + id + "']").attr('data-visibility','1');
            } else { 
              
				//$("#myTable").find("[data-td-id='" + id + "']").hide();	
				//$("#myTable").find("[data-th-id='" + id + "']").hide();	
				//$("#myTable #column_"+id).attr('data-visibility','0');
				$("#myTable [data-th-id='" + id + "']").attr('data-visibility','0');
				$("#myTable [data-td-id='" + id + "']").attr('data-visibility','0');
            }
			
			var data_col = $(this).attr('data-col');
			var data_field = $(this).attr('data-field');
			var data_table = $(this).attr('data-table');
			var data_position = $('#column_'+data_col).attr('data-col-position');
			var data_row = "0";
			var data_visibility = $("#myTable #column_"+id).attr('data-visibility');
			var data_value = '0';
			updatedatachild(data_field,data_table,template_id,data_value,data_col,data_position,data_row,data_visibility);
        });
		
		// change backcolor
		$(document).on("change",".changebackground",function(e){ 
            e.preventDefault();
			$(this).parent().parent().css('background',$(this).val());
			var field= $(this).attr('data-field');
			var table= $(this).attr('data-table');
			var data_type= $(this).attr('data-type');
			var serialized= $(this).attr('data-serialized');
			var value= $(this).val();
			updatedata(field, table, template_id, value, serialized, data_type);
		});

		// change backcolor
		$(document).on("change",".changebackground_th",function(e){ 
            e.preventDefault();
			$("#myTable").find("th").css('background',$(this).val());
			var field= $(this).attr('data-field');
			var table= $(this).attr('data-table');
			var data_type= $(this).attr('data-type');
			var serialized= $(this).attr('data-serialized');
			var value= $(this).val();
			updatedata(field, table, template_id, value, serialized, data_type);
		});
    
		function updatedata(field,table,template_id,value,serialized,data_type)
		{
			console.log(field+","+table+","+template_id+","+value+","+serialized+","+data_type);
			
			$.ajax({
				type: "POST",
				url: "update_data.php",
				data: "field="+field+"&node_task=edit&table="+table+"&id="+template_id+"&value="+value+
					  "&serialize="+serialized+"&type="+data_type,
				success: function(response)
				{
					console.log(response);
				}
			});
		}
		
		function updatedatachild(field,table,template_id,value,column_id,column_position,row_id,visibility)
		{
			console.log("field="+field+","+"table="+table+","+"template_id="+template_id+","+"value="+value+","+"column_id="+column_id+","+"column_position="+column_position+","+"row_id="+row_id+","+"visibility="+visibility);
		
			$.ajax({
				type: "POST",
				url: "update_data.php",
				data: "field="+field+"&table="+table+"&id="+template_id+"&value="+value+
					  "&column_id="+column_id+"&column_position="+column_position+"&row_id="+row_id+"&visibility="+visibility,
				success: function(response)
				{
					console.log(response);
				}
			});
		}
		
		// dat-table value		
		$(document).on("blur","#myTable td",function(e){ 
            e.preventDefault();
			var data_col = $(this).attr('data-col');
			var data_field = $(this).attr('data-field');
			var data_table = $(this).attr('data-table');
			var data_position = $('#column_'+data_col).attr('data-col-position');
			var data_row = $(this).attr('data-row');
			var data_visibility = $('#column_'+data_col).attr('data-visibility');
			var data_value = $(this).html();
			
		updatedatachild(data_field,data_table,template_id,data_value,data_col,data_position,data_row,data_visibility);
	 }); 
});
// final
   $('#inputFile').on('change', function() {
	    var reader = new FileReader();
        var file_data = $('#inputFile').prop('files')[0];
        var form_data = new FormData();  // Create a FormData object
        form_data.append('file', file_data);  // Append all element in FormData  object
        form_data.append('id',$('#inputFile').attr('data-template-id'));  // Append all element in FormData  object
 
        $.ajax({
                url         : 'upload_logo.php',     // point to server-side PHP script 
                dataType    : 'text',           // what to expect back from the PHP script, if anything
                cache       : false,
                contentType : false,
                processData : false,
                data        : form_data,                         
                type        : 'post',
                success     : function(output){
                  // alert(output);              // display response from the PHP script, if any
				   $('#image_upload_preview').attr('src', output);
                }
         });
         $('#image_upload_preview').val('');   
                  /* Clear the input type file */
		 
   }); 